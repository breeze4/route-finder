package route_finder.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import route_finder.core.Port;

public class AdjacencyGraphTests {

	private AdjacencyGraph<Port> graph;

	@Before
	public void setUp() throws Exception {
		graph = new AirportAdjacencyGraph();
	}

	@Test
	public void testDirectLinkingTwoVertices() {
		Port p1 = Port.port("ABQ");
		Port p2 = Port.port("DFW");
		graph.link(p1, p2);
		Assert.assertTrue(graph.isDirectlyLinked(p1, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p1));
	}

	@Test
	public void testDirectLinkingThreeVertices() {
		Port p1 = Port.port("ABQ");
		Port p2 = Port.port("DFW");
		Port p3 = Port.port("LAX");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p3, p1);
		Assert.assertTrue(graph.isDirectlyLinked(p1, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p1));
		Assert.assertTrue(graph.isDirectlyLinked(p1, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p1));
	}

	@Test
	public void testDirectLinkingFourVertices() {
		Port p1 = Port.port("ABQ");
		Port p2 = Port.port("DFW");
		Port p3 = Port.port("LAX");
		Port p4 = Port.port("HKG");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p3, p1);
		graph.link(p4, p3);
		Assert.assertTrue(graph.isDirectlyLinked(p1, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p1));
		Assert.assertTrue(graph.isDirectlyLinked(p1, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p1));
		Assert.assertTrue(graph.isDirectlyLinked(p4, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p4));
		Assert.assertFalse(graph.isDirectlyLinked(p4, p1));
		Assert.assertFalse(graph.isDirectlyLinked(p4, p2));
		Assert.assertFalse(graph.isDirectlyLinked(p1, p4));
		Assert.assertFalse(graph.isDirectlyLinked(p2, p4));
	}

	// p1 <-> p2 <-> p3
	@Test
	public void testIndirectLinkingThreeVertices_OneLinkBetween() {
		Port p1 = Port.port("ABQ");
		Port p2 = Port.port("DFW");
		Port p3 = Port.port("LAX");
		graph.link(p1, p2);
		graph.link(p3, p2);
		Assert.assertTrue(graph.isDirectlyLinked(p1, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p1));
		Assert.assertFalse(graph.isDirectlyLinked(p1, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p2));
		Assert.assertFalse(graph.isDirectlyLinked(p3, p1));
		Assert.assertTrue(graph.isLinked(p1, p2));
		Assert.assertTrue(graph.isLinked(p2, p1));
		Assert.assertTrue(graph.isLinked(p2, p3));
		Assert.assertTrue(graph.isLinked(p3, p2));
		Assert.assertTrue(graph.isLinked(p1, p3));
		Assert.assertTrue(graph.isLinked(p3, p1));
	}

	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testIndirectLinkingFourVertices_TwoLinksBetween() {
		Port p1 = Port.port("ABQ");
		Port p2 = Port.port("DFW");
		Port p3 = Port.port("LAX");
		Port p4 = Port.port("HKG");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		Assert.assertTrue(graph.isDirectlyLinked(p1, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p1));
		Assert.assertFalse(graph.isDirectlyLinked(p1, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p2));
		Assert.assertFalse(graph.isDirectlyLinked(p3, p1));
		Assert.assertTrue(graph.isLinked(p1, p2));
		Assert.assertTrue(graph.isLinked(p2, p1));
		Assert.assertTrue(graph.isLinked(p2, p3));
		Assert.assertTrue(graph.isLinked(p3, p2));
		Assert.assertTrue(graph.isLinked(p1, p3));
		Assert.assertTrue(graph.isLinked(p3, p1));
		Assert.assertFalse(graph.isDirectlyLinked(p1, p4));
		Assert.assertFalse(graph.isDirectlyLinked(p2, p4));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p4));
		Assert.assertTrue(graph.isDirectlyLinked(p4, p3));
		Assert.assertTrue(graph.isLinked(p3, p4));
		Assert.assertTrue(graph.isLinked(p4, p3));
		Assert.assertTrue(graph.isLinked(p2, p4));
		Assert.assertTrue(graph.isLinked(p4, p2));
		Assert.assertTrue(graph.isLinked(p1, p4));
		Assert.assertTrue(graph.isLinked(p4, p1));
	}

	// .......p5
	// .......|
	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testIndirectLinkingFiveVertices_TwoLinksBetween() {
		Port p1 = Port.port("ABQ");
		Port p2 = Port.port("DFW");
		Port p3 = Port.port("LAX");
		Port p4 = Port.port("HKG");
		Port p5 = Port.port("MIA");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		graph.link(p2, p5);
		Assert.assertTrue(graph.isDirectlyLinked(p1, p2));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p1));
		Assert.assertFalse(graph.isDirectlyLinked(p1, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p2, p3));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p2));
		Assert.assertFalse(graph.isDirectlyLinked(p3, p1));
		Assert.assertTrue(graph.isLinked(p1, p2));
		Assert.assertTrue(graph.isLinked(p2, p1));
		Assert.assertTrue(graph.isLinked(p2, p3));
		Assert.assertTrue(graph.isLinked(p3, p2));
		Assert.assertTrue(graph.isLinked(p1, p3));
		Assert.assertTrue(graph.isLinked(p3, p1));
		Assert.assertFalse(graph.isDirectlyLinked(p1, p4));
		Assert.assertFalse(graph.isDirectlyLinked(p2, p4));
		Assert.assertTrue(graph.isDirectlyLinked(p3, p4));
		Assert.assertTrue(graph.isDirectlyLinked(p4, p3));
		Assert.assertTrue(graph.isLinked(p3, p4));
		Assert.assertTrue(graph.isLinked(p4, p3));
		Assert.assertTrue(graph.isLinked(p2, p4));
		Assert.assertTrue(graph.isLinked(p4, p2));
		Assert.assertTrue(graph.isLinked(p1, p4));
		Assert.assertTrue(graph.isLinked(p4, p1));
		Assert.assertTrue(graph.isLinked(p5, p1));
		Assert.assertTrue(graph.isLinked(p5, p2));
		Assert.assertTrue(graph.isLinked(p5, p3));
		Assert.assertTrue(graph.isLinked(p5, p4));
		Assert.assertTrue(graph.isLinked(p1, p5));
		Assert.assertTrue(graph.isLinked(p2, p5));
		Assert.assertTrue(graph.isLinked(p3, p5));
		Assert.assertTrue(graph.isLinked(p4, p5));
	}

	// .......p5 <-> p6
	// .......|
	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testIndirectLinkingSixVertices_FourLinksBetween() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		Port p5 = Port.port("p5");
		Port p6 = Port.port("p6");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		graph.link(p2, p5);
		graph.link(p5, p6);
		Assert.assertTrue(graph.isLinked(p1, p6));
		Assert.assertTrue(graph.isLinked(p2, p6));
		Assert.assertTrue(graph.isLinked(p3, p6));
		Assert.assertTrue(graph.isLinked(p4, p6));
		Assert.assertTrue(graph.isLinked(p5, p6));
	}

	// .......p5 <-> p6
	// .......|......|
	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testIndirectLinkingSixVertices_Loop() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		Port p5 = Port.port("p5");
		Port p6 = Port.port("p6");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		graph.link(p2, p5);
		graph.link(p5, p6);
		graph.link(p3, p6);
		Assert.assertTrue(graph.isLinked(p1, p6));
		Assert.assertTrue(graph.isLinked(p2, p6));
		Assert.assertTrue(graph.isLinked(p3, p6));
		Assert.assertTrue(graph.isLinked(p4, p6));
		Assert.assertTrue(graph.isLinked(p5, p6));
	}

	// .......p7 <-> p8
	// .......|
	// .......p5 <-> p6
	// .......|......|
	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testIndirectLinkingEightVertices_Loop() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		Port p5 = Port.port("p5");
		Port p6 = Port.port("p6");
		Port p7 = Port.port("p7");
		Port p8 = Port.port("p8");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		graph.link(p2, p5);
		graph.link(p5, p6);
		graph.link(p3, p6);
		graph.link(p5, p7);
		graph.link(p7, p8);
		Assert.assertTrue(graph.isLinked(p1, p6));
		Assert.assertTrue(graph.isLinked(p2, p6));
		Assert.assertTrue(graph.isLinked(p3, p6));
		Assert.assertTrue(graph.isLinked(p4, p6));
		Assert.assertTrue(graph.isLinked(p5, p6));
		Assert.assertTrue(graph.isLinked(p7, p6));
		Assert.assertTrue(graph.isLinked(p8, p6));
		Assert.assertTrue(graph.isLinked(p8, p4));
	}

	// .......p7 <-> p8
	// .......|
	// .......p5 <-> p6
	// .......|......|
	// p1 <-> p2 <-> p3 <-> p4
	// .......|......|
	// .......p9 <-> p10
	@Test
	public void testIndirectLinkingTenVertices_TwoLoops() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		Port p5 = Port.port("p5");
		Port p6 = Port.port("p6");
		Port p7 = Port.port("p7");
		Port p8 = Port.port("p8");
		Port p9 = Port.port("p9");
		Port p10 = Port.port("p10");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		graph.link(p2, p5);
		graph.link(p5, p6);
		graph.link(p3, p6);
		graph.link(p5, p7);
		graph.link(p7, p8);
		graph.link(p9, p2);
		graph.link(p9, p10);
		graph.link(p10, p3);
		Assert.assertTrue(graph.isLinked(p1, p6));
		Assert.assertTrue(graph.isLinked(p2, p6));
		Assert.assertTrue(graph.isLinked(p3, p6));
		Assert.assertTrue(graph.isLinked(p4, p6));
		Assert.assertTrue(graph.isLinked(p5, p6));
		Assert.assertTrue(graph.isLinked(p7, p6));
		Assert.assertTrue(graph.isLinked(p8, p6));
		Assert.assertTrue(graph.isLinked(p8, p4));
		Assert.assertTrue(graph.isLinked(p4, p9));
		Assert.assertTrue(graph.isLinked(p8, p9));
		Assert.assertTrue(graph.isLinked(p8, p10));
	}

	// p1 <-> p2 <-> p3
	@Test
	public void testRoutesBetweenThreeVertices() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		graph.link(p1, p2);
		graph.link(p3, p2);
		Assert.assertTrue(graph.isLinked(p1, p3));
		ArrayList<Port> expected = new ArrayList<Port>();
		expected.add(p1);
		expected.add(p2);
		expected.add(p3);
		Set<List<Port>> expectedRoutes = new HashSet<List<Port>>();
		expectedRoutes.add(expected);
		Set<List<Port>> actualRoutes = graph.findRoutes(p1, p3);
		compareSetsOfLists(expectedRoutes, actualRoutes);
	}

	// .......p4
	// .......|
	// p1 <-> p2 <-> p3
	@Test
	public void testRoutesBetweenFourVertices() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p2);
		Assert.assertTrue(graph.isLinked(p1, p4));
		List<Port> expected = new ArrayList<Port>();
		expected.add(p1);
		expected.add(p2);
		expected.add(p4);
		Set<List<Port>> expectedRoutes = new HashSet<List<Port>>();
		expectedRoutes.add(expected);
		Set<List<Port>> actualRoutes = graph.findRoutes(p1, p4);
		compareSetsOfLists(expectedRoutes, actualRoutes);
	}

	// .......p5 <-> p6
	// .......|......|
	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testRoutesBetweenSixVertices_WithLoop() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		Port p5 = Port.port("p5");
		Port p6 = Port.port("p6");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p4, p3);
		graph.link(p2, p5);
		graph.link(p6, p5);
		graph.link(p6, p3);
		Assert.assertTrue(graph.isLinked(p1, p6));
		List<Port> expected1 = Arrays.asList(p1, p2, p5, p6);
		List<Port> expected2 = Arrays.asList(p1, p2, p3, p6);
		Set<List<Port>> expectedRoutes = new HashSet<List<Port>>();
		expectedRoutes.add(expected1);
		expectedRoutes.add(expected2);
		Set<List<Port>> actualRoutes = graph.findRoutes(p1, p6);
		compareSetsOfLists(expectedRoutes, actualRoutes);
	}

	// .......p5 <-> p6
	// .......|......|
	// p1 <-> p2 <-> p3 <-> p4
	@Test
	public void testRoutesBetweenSixVertices_WithLoopDifferentSearch() {
		Port p1 = Port.port("p1");
		Port p2 = Port.port("p2");
		Port p3 = Port.port("p3");
		Port p4 = Port.port("p4");
		Port p5 = Port.port("p5");
		Port p6 = Port.port("p6");
		graph.link(p1, p2);
		graph.link(p3, p2);
		graph.link(p3, p4);
		graph.link(p2, p5);
		graph.link(p6, p5);
		graph.link(p6, p3);
		Assert.assertTrue(graph.isLinked(p1, p6));
		List<Port> expected1 = Arrays.asList(p4, p3, p6, p5);
		List<Port> expected2 = Arrays.asList(p4, p3, p2, p5);
		Set<List<Port>> expectedRoutes = new HashSet<List<Port>>();
		expectedRoutes.add(expected1);
		expectedRoutes.add(expected2);
		Set<List<Port>> actualRoutes = graph.findRoutes(p4, p5);
		compareSetsOfLists(expectedRoutes, actualRoutes);
	}

	private void compareSetsOfLists(Set<List<Port>> expectedRoutes, Set<List<Port>> actualRoutes) {
		boolean matchFound = false;
		for (List<Port> actualRoute : actualRoutes) {
			for (List<Port> expectedRoute : expectedRoutes) {
				boolean routeFound = assertExpectedRouteFound(expectedRoute, actualRoute);
				if (routeFound) {
					matchFound = true;
					break;
				}
			}
		}
		Assert.assertTrue(matchFound);
	}

	private boolean assertExpectedRouteFound(List<Port> expected, List<Port> actualRoutes) {
		if (expected.size() != actualRoutes.size()) {
			return false;
		}
		return Arrays.deepEquals(expected.toArray(), actualRoutes.toArray());
	}
}
