package route_finder.schedule_parsing;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import route_finder.schedule_creator.ScheduleRepository;
import route_finder.schedules.AmericanAirlinesSchedule;
import route_finder.schedules.Schedule;

public class ParseAmericanAirlinesScheduleTest {

	JdbcTemplate jdbcTemplate;
	ScheduleRepository scheduleRepository;

	@Before
	public void setUp() throws Exception {
		EmbeddedDatabase db = new EmbeddedDatabaseBuilder().setName("parserTestDb").setType(EmbeddedDatabaseType.H2)
				.addScript("schema.sql").addScript("data.sql").build();
		jdbcTemplate = new JdbcTemplate(db);
		scheduleRepository = new ScheduleRepository(jdbcTemplate);
	}

	@Test
	public void testBasicParse() throws Exception {
		// Create the CSVFormat object
		CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');

		// initialize the CSVParser object
		CSVParser parser = new CSVParser(new FileReader(
				"src/main/resources/schedules/american_airlines/epx_cargoschedule_20Oct14.csv"), format);

		List<AmericanAirlinesSchedule> schedules = new ArrayList<>();
		for (CSVRecord record : parser) {
			String origin = record.get("Column1");
			String destination = record.get("Column2");
			String effectiveDateStart = record.get("Column8");
			if (StringUtils.isNotEmpty(origin) && StringUtils.isNotEmpty(destination)
					&& StringUtils.isNotEmpty(effectiveDateStart)) {
				AmericanAirlinesSchedule americanAirlinesSchedule = new AmericanAirlinesSchedule(origin, destination,
						record.get("Column3"), record.get("Column4"), record.get("Column5"), record.get("Column6"),
						record.get("Column7"), effectiveDateStart, record.get("Column9"));
				schedules.add(americanAirlinesSchedule);
				System.out.println(americanAirlinesSchedule);
				scheduleRepository.saveSchedule(americanAirlinesSchedule);
			}
		}
		List<Schedule> retrievedSchedules = scheduleRepository.getSchedulesForLane("ABQ", "DFW");
		for (Schedule schedule : retrievedSchedules) {
			System.out.println(schedule);
		}
		parser.close();
	}

}
