package route_finder.core;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

public class Port {

	private final String portCode;

	private Port(String portCode) {
		Assert.hasLength(portCode);
		this.portCode = portCode;
	}

	public static Port port(String portCode) {
		return StringUtils.isEmpty(portCode) ? null : new Port(portCode);
	}

	public static String getCode(Port port) {
		return port == null ? null : port.portCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((portCode == null) ? 0 : portCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Port other = (Port) obj;
		if (portCode == null) {
			if (other.portCode != null) {
				return false;
			}
		} else if (!portCode.equals(other.portCode)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Port[" + portCode + "]";
	}
}
