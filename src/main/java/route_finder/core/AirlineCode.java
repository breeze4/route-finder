package route_finder.core;

import org.apache.commons.lang3.StringUtils;

public enum AirlineCode {

	AAL, DAL, UAL;

	public static AirlineCode airlineCode(String code) {
		return StringUtils.isNotEmpty(code) ? valueOf(code) : null;
	}
}
