package route_finder.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import route_finder.schedule_creator.ScheduleCreator;
import route_finder.schedule_creator.ScheduleManager;
import route_finder.schedule_creator.ScheduleRepository;
import route_finder.schedules.Schedule;

@Controller
@EnableAutoConfiguration
public class ScheduleController {

	@Inject
	ScheduleRepository scheduleRepository;

	@Inject
	ScheduleManager scheduleManager;

	@RequestMapping("/schedules/{origin}/{destination}")
	@ResponseBody
	List<Schedule> getlaneSchedule(@PathVariable String origin, @PathVariable String destination) {
		System.out.println("Retrieving schedule for lane: " + origin + "-" + destination);

		List<Schedule> schedulesForLane = null;

		if ("ALL".equals(origin) && !"ALL".equals(destination)) {
			schedulesForLane = scheduleRepository.getSchedulesToDestination(destination);
		} else if ("ALL".equals(destination) && !"ALL".equals(origin)) {
			schedulesForLane = scheduleRepository.getSchedulesFromOrigin(origin);
		} else if ("ALL".equals(destination) && "ALL".equals(origin)) {
			schedulesForLane = scheduleRepository.getAllSchedules();
		} else {
			schedulesForLane = scheduleRepository.getSchedulesForLane(origin, destination);
		}

		return schedulesForLane;
	}

	@RequestMapping(value = "/schedules/populate/{airlineCode}", method = RequestMethod.POST)
	@ResponseBody
	void populateSchedules(@PathVariable String airlineCode) throws FileNotFoundException, IOException {
		scheduleRepository.clearSchedules(airlineCode);
		ScheduleCreator creator = scheduleManager.getScheduleCreatorForAirline(airlineCode);
		creator.populateSchedules();
	}
}