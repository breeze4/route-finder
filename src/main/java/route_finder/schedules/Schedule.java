package route_finder.schedules;

public class Schedule {

	private String origin;
	private String destination;
	private String flightNumber;
	private String frequency;
	private String departureTime;
	private String arrivalTime;
	private String aircraftType;
	private String effectiveDateStart;
	private String effectiveDateEnd;
	private String airlineName;
	private String airlineCode;

	public Schedule(String origin, //
			String destination, //
			String flightNumber, //
			String frequency, //
			String departureTime, //
			String arrivalTime, //
			String aircraftType, //
			String effectiveDateStart, //
			String effectiveDateEnd, //
			String airlineName, //
			String airlineCode) {
		this.origin = origin;
		this.destination = destination;
		this.flightNumber = flightNumber;
		this.frequency = frequency;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.aircraftType = aircraftType;
		this.effectiveDateStart = effectiveDateStart;
		this.effectiveDateEnd = effectiveDateEnd;
		this.airlineName = airlineName;
		this.airlineCode = airlineCode;
	}

	public String getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public String getFrequency() {
		return frequency;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public String getEffectiveDateStart() {
		return effectiveDateStart;
	}

	public String getEffectiveDateEnd() {
		return effectiveDateEnd;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	@Override
	public String toString() {
		return "AmericanAirlinesSchedule [origin=" + origin + ", destination=" + destination + ", flightNumber="
				+ flightNumber + ", frequency=" + frequency + ", departureTime=" + departureTime + ", arrivalTime="
				+ arrivalTime + ", aircraftType=" + aircraftType + ", effectiveDateStart=" + effectiveDateStart
				+ ", effectiveDateEnd=" + effectiveDateEnd + ", airlineName=" + airlineName + ", airlineCode="
				+ airlineCode + "]";
	}
}
