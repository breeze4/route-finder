package route_finder.schedules;

public class AmericanAirlinesSchedule extends Schedule {

	public AmericanAirlinesSchedule(String origin, //
			String destination, //
			String flightNumber, //
			String frequency, //
			String departureTime, //
			String arrivalTime, //
			String aircraftType, //
			String effectiveDateStart, //
			String effectiveDateEnd) {
		super(origin, destination, flightNumber, frequency, departureTime, arrivalTime, aircraftType,
				effectiveDateStart, effectiveDateEnd, "American Airlines", "AAL");
	}
}
