package route_finder.schedule_creator;

import static route_finder.core.AirlineCode.airlineCode;

import java.util.EnumMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import route_finder.core.AirlineCode;

@Component
public class ScheduleManager {

	@Inject
	private ScheduleRepository scheduleRepository;

	private final Map<AirlineCode, ScheduleCreator> creators = new EnumMap<AirlineCode, ScheduleCreator>(
			AirlineCode.class);

	private ScheduleManager() {
	}

	public ScheduleCreator getScheduleCreatorForAirline(String airlineCode) {

		ScheduleCreator scheduleCreator = creators.get(airlineCode(airlineCode));
		if (scheduleCreator == null) {
			scheduleCreator = new AmericanAirlinesScheduleCreator(scheduleRepository);
			creators.put(AirlineCode.AAL, scheduleCreator);
		}
		return scheduleCreator;
	}

}
