package route_finder.schedule_creator;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import route_finder.schedules.AmericanAirlinesSchedule;

public class AmericanAirlinesScheduleCreator implements ScheduleCreator {

	private final ScheduleRepository scheduleRepository;

	private final static String AIRLINE_CODE = "AAL";

	public AmericanAirlinesScheduleCreator(ScheduleRepository scheduleRepository) {
		this.scheduleRepository = scheduleRepository;
	}

	@Override
	public void populateSchedules() {
		// Create the CSVFormat object
		CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');

		// initialize the CSVParser object
		CSVParser parser;
		try {
			parser = new CSVParser(new FileReader(
					"src/main/resources/schedules/american_airlines/epx_cargoschedule_20Oct14.csv"), format);
			List<AmericanAirlinesSchedule> schedules = new ArrayList<>();
			for (CSVRecord record : parser) {
				String origin = record.get("Column1");
				String destination = record.get("Column2");
				String effectiveDateStart = record.get("Column8");
				if (StringUtils.isNotEmpty(origin) && StringUtils.isNotEmpty(destination)
						&& StringUtils.isNotEmpty(effectiveDateStart)) {
					AmericanAirlinesSchedule americanAirlinesSchedule = new AmericanAirlinesSchedule(origin,
							destination, record.get("Column3"), record.get("Column4"), record.get("Column5"),
							record.get("Column6"), record.get("Column7"), effectiveDateStart, record.get("Column9"));
					schedules.add(americanAirlinesSchedule);
					System.out.println(americanAirlinesSchedule);
					scheduleRepository.saveSchedule(americanAirlinesSchedule);
				}
			}
			System.out.println("Successfully populated " + schedules.size() + " schedules for " + AIRLINE_CODE);
			parser.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
