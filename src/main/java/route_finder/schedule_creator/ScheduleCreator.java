package route_finder.schedule_creator;

public interface ScheduleCreator {

	public void populateSchedules();
}