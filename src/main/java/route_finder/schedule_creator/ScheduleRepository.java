package route_finder.schedule_creator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import route_finder.schedules.AmericanAirlinesSchedule;
import route_finder.schedules.Schedule;

@Component
public class ScheduleRepository {

	private static final String INSERT_SCHEDULE = "INSERT INTO ROUTES.SCHEDULES values(DEFAULT, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_LANE_SCHEDULES = "select * from routes.schedules where origin = ? and destination = ?";
	private static final String GET_ALL_LANE_SCHEDULES = "select * from routes.schedules";
	private static final String GET_ORIGIN_SCHEDULES = "select * from routes.schedules where origin = ?";
	private static final String GET_DESTINATION_SCHEDULES = "select * from routes.schedules where destination = ?";
	private static final String CLEAR_AIRLINE_SCHEDULE = "delete from routes.schedules where airline_code = ?";
	private JdbcTemplate jdbcTemplate;

	@Inject
	public ScheduleRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void saveSchedule(Schedule schedule) {
		jdbcTemplate.update(INSERT_SCHEDULE, schedule.getOrigin(), schedule.getDestination(),
				schedule.getFlightNumber(), schedule.getFrequency(), schedule.getDepartureTime(),
				schedule.getArrivalTime(), schedule.getAircraftType(), schedule.getEffectiveDateStart(),
				schedule.getEffectiveDateEnd(), schedule.getAirlineCode());
	}

	public List<Schedule> getSchedulesForLane(String origin, String destination) {
		return jdbcTemplate.query(GET_LANE_SCHEDULES, new ResultSetExtractor<List<Schedule>>() {

			@Override
			public List<Schedule> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Schedule> list = new ArrayList<Schedule>();
				while (rs.next()) {
					AmericanAirlinesSchedule schedule = new AmericanAirlinesSchedule(rs.getString(2), rs.getString(3),
							rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs
							.getString(9), rs.getString(10));
					list.add(schedule);
				}
				return list;
			}
		}, origin, destination);
	}

	public List<Schedule> getSchedulesFromOrigin(String origin) {
		return jdbcTemplate.query(GET_ORIGIN_SCHEDULES, new ResultSetExtractor<List<Schedule>>() {

			@Override
			public List<Schedule> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Schedule> list = new ArrayList<Schedule>();
				while (rs.next()) {
					AmericanAirlinesSchedule schedule = new AmericanAirlinesSchedule(rs.getString(2), rs.getString(3),
							rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs
							.getString(9), rs.getString(10));
					list.add(schedule);
				}
				return list;
			}
		}, origin);
	}

	public List<Schedule> getSchedulesToDestination(String destination) {
		return jdbcTemplate.query(GET_DESTINATION_SCHEDULES, new ResultSetExtractor<List<Schedule>>() {

			@Override
			public List<Schedule> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Schedule> list = new ArrayList<Schedule>();
				while (rs.next()) {
					AmericanAirlinesSchedule schedule = new AmericanAirlinesSchedule(rs.getString(2), rs.getString(3),
							rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs
							.getString(9), rs.getString(10));
					list.add(schedule);
				}
				return list;
			}
		}, destination);
	}

	public void clearSchedules(String airlineCode) {
		jdbcTemplate.update(CLEAR_AIRLINE_SCHEDULE, airlineCode);

	}

	public List<Schedule> getAllSchedules() {
		return jdbcTemplate.query(GET_ALL_LANE_SCHEDULES, new ResultSetExtractor<List<Schedule>>() {

			@Override
			public List<Schedule> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Schedule> list = new ArrayList<Schedule>();
				while (rs.next()) {
					AmericanAirlinesSchedule schedule = new AmericanAirlinesSchedule(rs.getString(2), rs.getString(3),
							rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs
							.getString(9), rs.getString(10));
					list.add(schedule);
				}
				return list;
			}
		});
	}
}
