package route_finder.graph;

import java.util.HashSet;
import java.util.Set;

public class Vertex {

	private final Set<Vertex> adjacents = new HashSet<Vertex>();

	public Vertex() {

	}

	public void addAdjacent(Vertex adj) {
		adjacents.add(adj);
	}

	public boolean adjacent(Vertex adj) {
		return adjacents.contains(adj);
	}

}
