package route_finder.graph;

import route_finder.core.Port;

public class Lane {

	private final Port origin;
	private final Port destination;

	public Lane(Port origin, Port destination) {
		this.origin = origin;
		this.destination = destination;
	}

	public Port getOrigin() {
		return origin;
	}

	public Port getDestination() {
		return destination;
	}
}
