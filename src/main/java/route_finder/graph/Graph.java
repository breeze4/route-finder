package route_finder.graph;

import java.util.List;
import java.util.Set;

/**
 * @author Aniara
 *
 * @param <V>
 */
public interface Graph<V> {

	/**
	 * Checks if two vertices are directly linked.
	 *
	 * @param start
	 * @param end
	 * @return true if v1 and v2 are directly linked
	 */
	public boolean isDirectlyLinked(V start, V end);

	/**
	 * Checks if two vertices are linked through any number of other vertices.
	 *
	 * @param start
	 * @param end
	 * @return true if v1 and v2 are linked
	 */
	public boolean isLinked(V start, V end);

	public void link(V v1, V v2);

	public Set<List<V>> findRoutes(V start, V end);

}
