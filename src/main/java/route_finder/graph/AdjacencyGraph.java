package route_finder.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AdjacencyGraph<V> implements Graph<V> {

	private final Map<V, Set<V>> graph = new HashMap<V, Set<V>>();
	private static final Logger LOGGER = LoggerFactory.getLogger(AdjacencyGraph.class);

	@Override
	public void link(V start, V end) {
		Set<V> adjacents1 = graph.get(start);
		if (adjacents1 == null) {
			adjacents1 = new HashSet<V>();
		}
		adjacents1.add(end);
		graph.put(start, adjacents1);
		Set<V> adjacents2 = graph.get(end);
		if (adjacents2 == null) {
			adjacents2 = new HashSet<V>();
		}
		adjacents2.add(start);
		graph.put(end, adjacents2);
	}

	@Override
	public boolean isDirectlyLinked(V start, V end) {
		Set<V> v1Adj = graph.get(start);
		boolean v1ContainsV2 = false;
		if (v1Adj != null) {
			v1ContainsV2 = v1Adj.contains(end);
		}
		Set<V> v2Adj = graph.get(end);
		boolean v2ContainsV1 = false;
		if (v2Adj != null) {
			v2ContainsV1 = v2Adj.contains(start);
		}

		return v1ContainsV2 && v2ContainsV1;
	}

	@Override
	public boolean isLinked(V start, V end) {
		LOGGER.info("--- Searching for links between {} and {} ---", start, end);
		return isLinkedByLane(null, start, end);
	}

	private boolean isLinkedByLane(V parent, V current, V target) {
		boolean linkFound = isDirectlyLinked(current, target);
		if (linkFound) {
			return linkFound;
		}

		Set<V> unvisitedAdjacents = new HashSet<V>(graph.get(current));
		unvisitedAdjacents.remove(parent);
		for (V adj : unvisitedAdjacents) {
			linkFound = isLinkedByLane(current, adj, target);
			if (linkFound) {
				return linkFound;
			}
		}

		return linkFound;
	}

	private List<V> findRoutes(V parent, V current, V target, List<V> routeSoFar, Set<List<V>> routesFound) {
		boolean linkFound = isDirectlyLinked(current, target);
		if (linkFound) {
			routeSoFar.add(current);
			routeSoFar.add(target);
			return routeSoFar;
		}

		Set<V> unvisitedAdjacents = new HashSet<V>(graph.get(current));
		unvisitedAdjacents.remove(parent);
		List<V> result = null;
		if (unvisitedAdjacents.size() != 0) {
			routeSoFar.add(current);
		}
		for (V next : unvisitedAdjacents) {
			result = findRoutes(current, next, target, new ArrayList<V>(routeSoFar), routesFound);
			if (result.contains(target)) {
				routesFound.add(result);
			}
		}
		return new ArrayList<V>();
	}

	@Override
	public Set<List<V>> findRoutes(V start, V end) {
		LOGGER.info("--- Searching for routes between {} and {} ---", start, end);
		Set<List<V>> routesFound = new HashSet<List<V>>();
		findRoutes(null, start, end, new ArrayList<V>(), routesFound);
		LOGGER.info("Found routes: {}", routesFound);
		return routesFound;
	}

}
