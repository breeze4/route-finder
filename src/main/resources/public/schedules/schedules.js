'use strict';

var module = angular.module('routeFinder.schedules', [ 'ngRoute', 'ngResource' ]);

module.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/schedules', {
	    templateUrl : '/schedules/schedules.html',
	    controller : 'SchedulesCtrl'
	});
} ]);

module.factory('SchedulesFactory', [ '$resource', function($resource) {
	return $resource('/schedules/:origin/:destination');
} ]);

module.controller('SchedulesCtrl', [ '$scope', 'SchedulesFactory', function($scope, SchedulesFactory) {

	$scope.criteria = {
	    origin : 'ABQ',
	    destination : 'DFW'
	};

	$scope.getSchedules = function getSchedules() {
		var criteria = {
		    origin : $scope.criteria.origin || 'ALL',
		    destination : $scope.criteria.destination || 'ALL'
		};
		SchedulesFactory.query(criteria, function success(response) {
			$scope.schedules = response;
		}, function error() {
		});
	};

} ]);