'use strict';

// Declare app level module which depends on views, and components
var module = angular.module('routeFinder', [ 'ngRoute', 'routeFinder.schedules', 'routeFinder.admin' ]).config(
        [ '$routeProvider', function($routeProvider) {
	        $routeProvider.otherwise({
		        redirectTo : '/schedules'
	        });
        } ]);

module.controller('NavCtrl', [ '$scope', '$location', function($scope, $location) {

	$scope.isActive = function(viewLocation) {
		return viewLocation === $location.path();
	};

} ]);