'use strict';

var module = angular.module('routeFinder.admin', [ 'ngRoute', 'ngResource' ]);

module.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/admin', {
	    templateUrl : '/admin/admin.html',
	    controller : 'AdminCtrl'
	});
} ]);

module.factory('SchedulesPopulator', [ '$resource', function($resource) {
	return $resource('/schedules/populate/:airlineCode');
} ]);

module.controller('AdminCtrl', [ '$scope', 'SchedulesPopulator', function($scope, SchedulesPopulator) {

	$scope.populateSchedules = function populateSchedules() {
		SchedulesPopulator.save({
			airlineCode : 'AAL'
		}, {}, function success() {
		}, function error() {
		});
	};
} ]);