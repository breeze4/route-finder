create sequence if not exists route_id_seq;

create table if not exists ROUTES.SCHEDULES (
id bigint default route_id_seq.nextval primary key,
ORIGIN VARCHAR(3) not null,
DESTINATION VARCHAR(3) not null,
FLIGHT_NUMBER VARCHAR(4) not null,
FREQUENCY VARCHAR(8) not null,
DEPARTURE_TIME VARCHAR(5) not null,
ARRIVAL_TIME VARCHAR(5) not null,
AIRCRAFT_TYPE VARCHAR(5),
EFFECTIVE_DATE_START VARCHAR(10) not null,
EFFECTIVE_DATE_END VARCHAR(10),
AIRLINE_CODE VARCHAR(5) not null
);

create index origin on routes.schedules (origin);

create index destination on routes.schedules (destination);

create index FLIGHT_NUMBER on routes.schedules (FLIGHT_NUMBER);

create index DEPARTURE_TIME on routes.schedules (DEPARTURE_TIME);

create index ARRIVAL_TIME on routes.schedules (ARRIVAL_TIME);

create index FREQUENCY on routes.schedules (FREQUENCY);